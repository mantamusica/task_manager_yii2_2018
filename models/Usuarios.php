<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string $usuario
 * @property string $password
 * @property string $codigo
 * @property string $email
 * @property int $active
 *
 * @property Chores[] $chores
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'codigo'], 'required'],
            [['id', 'active'], 'integer'],
            [['usuario', 'password'], 'string', 'max' => 255],
            [['codigo'], 'string', 'max' => 256],
            [['email'], 'string', 'max' => 50],
            [['id'], 'unique'],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'User',
            'password' => 'Password',
            'codigo' => 'Codigo',
            'email' => 'Email',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChores()
    {
        return $this->hasMany(Chores::className(), ['id_user' => 'id']);
    }

    /**
     * @inheritdoc
     * @return UsuariosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsuariosQuery(get_called_class());
    }
}
