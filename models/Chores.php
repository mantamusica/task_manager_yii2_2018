<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chores".
 *
 * @property int $id
 * @property string $name
 * @property string $expiration_date
 * @property string $creation_date
 * @property int $alarm
 * @property int $active
 * @property int $id_user
 * @property int $id_category
 *
 * @property Categories $category
 * @property Usuarios $user
 */
class Chores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'expiration_date', 'creation_date', 'alarm', 'active', 'id_user', 'id_category'], 'required'],
            [['expiration_date', 'creation_date'], 'safe'],
            [['alarm', 'active', 'id_user', 'id_category'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['id_category'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['id_category' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'expiration_date' => 'Expiration Date',
            'creation_date' => 'Creation Date',
            'alarm' => 'Alarm',
            'active' => 'Active',
            'id_user' => 'Id User',
            'id_category' => 'Id Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'id_user']);
    }

    /**
     * @inheritdoc
     * @return ChoresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChoresQuery(get_called_class());
    }
}
