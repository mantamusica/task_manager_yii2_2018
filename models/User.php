<?php

namespace app\models;

use Yii;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    public $password_repeat;
    public $codigo;
    

    public static function tableName() {
        return 'usuarios';
    }

    public function rules() {
        return [
            [['usuario', 'password'], 'string', 'max' => 255],
            [['usuario', 'password', 'password_repeat'], 'required', 'message' => 'El campo {attribute} es obligatorio'],
            // que el usuario no exista
            ['usuario', 'unique', 'message' => 'El usuario ya existe en el sistema'],
            ['password', 'string', 'min' => 4, 'message' => 'la contraseña debe tener al menos 6 caracteres'],
            //comparacion de contraseñas
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'message' => 'Las contraseñas deben coincidir'],
            // validacion de captcha
            //['codigo', 'captcha', 'message' => 'No coincide el codigo mostrado'], // esto funcionaria correctamente sin AJAX
            //Con AJAX existe un bug lo corregimos con una funcion
            ['codigo', 'codeVerify'],
            [['email'], 'string', 'max' => 50],
            ['codigo', 'required', 'message' => 'Debes escribir algo en los codigos'],
            //colocar los campos que necesito que pase en la asignacion masiva
            [['password_repeat', 'password', 'usuario', 'codigo'], 'safe'],
            [['active'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'usuario' => 'Username',
            'password' => 'Password',
            'password_repeat' => 'Repeat password',
            'email' => 'Email',
            'active' => 'Active',
            'codigo' => 'Write the codes you see'
        ];
    }

    public function codeVerify($attribute) {
        /* nombre de la accion del controlador */
        $captcha_validate = new \yii\captcha\CaptchaAction('captcha', Yii::$app->controller);


        if ($this->$attribute) {
            $code = $captcha_validate->getVerifyCode();
            if ($this->$attribute != $code) {
                $this->addError($attribute, 'That verification code is not correct');
            }
        }
    }

    public static function findIdentity($id) {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username) {
        return static::findOne(['usuario' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /* public function validatePassword($password)
      {
      return Yii::$app->security->validatePassword($password, $this->password_hash);
      } */

//    public function validatePassword($password) {
//        return $this->password === $password;
//    }

    public function validatePassword($password) {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return null;
    }

    public function beforeSave($insert) {

        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {

                $this->authKey = \Yii::$app->security->generateRandomString();
                $this->password = Yii::$app->security->generatePasswordHash($this->password);
            }

            return true;
        }

        return false;
    }

    public function afterSave($insert) {
        // entra solamente si es insercion y no actualizacion
        if ($insert) {
            $body = "<h1>Haga click en el siguiente enlace para finalizar tu registro</h1>";
            $body .= "<a href='" . Yii::$app->params["direccion"] . Yii::$app->request->baseUrl . "/index.php/front/confirm?id=" . urlencode($this->id) . "&authKey=" . urlencode($this->authKey) . "'>Confirmar</a>";
            Yii::$app->mailer->compose()
                    ->setTo($this->email)
                    ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["titulo"]])
                    ->setSubject("Terminar el registro")
                    ->setHtmlBody($body)
                    ->send();
        }
    }
}
