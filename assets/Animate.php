<?php
namespace app\assets;

use yii\web\AssetBundle;

class Animate extends AssetBundle 
{
    
    public $basePath = '@webroot';
    public $baseUrl = '@web';
        
    public $css = [
        'css/animate.css'
    ];
    
    public $js = [
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        '\rmrevin\yii\fontawesome\AssetBundle',
    ];
}  

