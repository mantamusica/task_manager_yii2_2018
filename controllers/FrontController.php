<?php

/*
 * Controlador principal
 *
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\User;
use yii\widgets\ActiveForm;

class FrontController extends Controller {

    // vamos a controlar las reglas de acceso
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['contact'],
                'rules' => [
                    [
                        'actions' => ['contact', 'chores', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null, //con esto preguntamos que el entorno está en modo test
            ],
        ];
    }

    public function actionIndex() {
        $this->layout = "frontLayout";
        // cargamos la pagina de inicio
        return $this->render('index');
    }

    public function actionLogin($id = 0) {
        // en caso de no estar logueado nos colocamos en la pagina de inicio
        $this->layout = "frontLayout";
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        // en caso de intentar realizar un logueo
        if ($id == 0) {
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                // si es correcto volvemos a la pagina anterior
                return $this->goBack();
            }

            // en caso de que el logueo no sea correcto no entramos
            return $this->renderPartial('login', [
                        'model' => $model,
            ]);
        } else {

            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        // nos salimos de la sesion
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact() {
        $this->layout = "frontLayout";
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('Email');
            //evitar envio masivo del correo con F5
            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    public function actionRegistrar() {
        $this->layout = "frontLayout";
        $model = new User();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        /* crear el usuario */
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->goHome();
        }

        return $this->render('registro', [
                    'model' => $model,
        ]);
    }

    public function actionAjax() {
        $model = new User();

        /* validar por AJAX */
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        } else {
            return $this->renderAjax('registro', [
                        'model' => $model,
            ]);
        }
    }

    public function actionChema() {
        $this->redirect(['site/index']);
    }
    
    public function actionConfirm() {
        if (Yii::$app->request->get()) {

            //Obtenemos el valor de los parámetros get
            $id = \yii\helpers\Html::decode($_GET["id"]);
            $authKey = \yii\helpers\Html::decode($_GET["authKey"]);

            if ((int) $id) {
                //Realizamos la consulta para obtener el registro
                $model = User::find()
                        ->where("id=:id", [":id" => $id])
                        ->andWhere("authKey=:authKey", [":authKey" => $authKey]);
                
                //Si el registro existe
                if ($model->count() == 1) {
                    $activar = User::findOne($id);
                    $activar->active = 1;
//                    $activar->scenario="actualizar";
                    
                    if ($activar->save(false)) {
                        echo "Enhorabuena registro llevado a cabo correctamente, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; " . \yii\helpers\Url::to(["index"]) . "'>";
                    } else {
                        echo "Ha ocurrido un error al realizar el registro, redireccionando ...";
                        echo "<meta http-equiv='refresh' content='8; " . \yii\helpers\Url::to(["index"]) . "'>";
                    }
                } else { //Si no existe redireccionamos a login
                    return $this->redirect(["index"]);
                }
            } else { //Si id no es un número entero redireccionamos a login
                return $this->redirect(["index"]);
            }
        }
    }

}
