<?php

/*
 * Controlador principal
 *
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use yii\widgets\ActiveForm;

class BackController extends Controller {

    // vamos a controlar las reglas de acceso
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [''],
                'rules' => [
                    [
                        'actions' => ['chores', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null, //con esto preguntamos que el entorno está en modo test
            ],
        ];
    }

    public function init() {
        parent::init();
        $this->layout = "backLayout";
    }

    public function entrar() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (Yii::$app->user->identity->usuario != "admin") {
            return $this->goHome();
        }
    }

    public function actionIndex() {
        $this->entrar();
        return $this->render("index");
    }

    public function actionLogin() {
        // en caso de no estar logueado nos colocamos en la pagina de inicio

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(["back/index"]);
        }

        $model = new LoginForm();
        // en caso de intentar realizar un logueo
        if ($model->load(Yii::$app->request->post()) && $model->admin()) {

            return $this->redirect(["back/index"]);
        }

        // en caso de que el logueo no sea correcto no entramos
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionLogout() {
        // nos salimos de la sesion
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /* accion para enviar un correo con formulario */

    public function actionContact($id) {
        $model = new ContactForm();
        $usuario=User::findIdentity($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->contact()) {
            Yii::$app->session->setFlash('Email');
            //evitar envio masivo del correo con F5
            return $this->goHome();
        }
        
        $model->name=$usuario->usuario;
        $model->email=$usuario->email;
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }
    

    public function actionRegistrar() {
        $model = new User();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        /* crear el usuario */
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }

        return $this->render('registro', [
                    'model' => $model,
        ]);
    }

    public function actionAjax() {
        $model = new User();

        /* validar por AJAX */
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        } else {
            return $this->renderAjax('registro', [
                        'model' => $model,
            ]);
        }
    }

    public function actionChema() {
        $this->redirect(['site/index']);
    }

    public function actionFormSubmission() {
        $security = new Security();
        $string = Yii::$app->request->post('string');
        $stringHash = '';
        if (!is_null($string)) {
            $stringHash = $security->generatePasswordHash($string);
        }
        return $this->render('form-submission', [
                    'stringHash' => $stringHash,
        ]);
    }

    public function actionAutoRefresh() {
        return $this->render('auto-refresh', ['time' => date('H:i:s')]);
    }

}
