<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class Foto
 * @author chema
 */
class Foto extends Widget
{
    public $nombre = 'image.png';
    public $alternativo = "image";

    public function init(){
        parent::init();
    }

    public function run(){
        return Html::img("@web/imgs/$this->nombre", ["alt"=>$this->alternativo]);
    }
}
