<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Chores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_user')->dropDownList(
            ArrayHelper::map((new \yii\db\Query())->select(['id', 'usuario'])->from('usuarios')->all(), 'id', 'usuario'))
    ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'expiration_date')->widget(DateControl::classname(), [
        'displayFormat' => 'php:Y-m-d H:m:s',
        'type' => DateControl::FORMAT_DATETIME
    ]);
    ?>

    <?=
    $form->field($model, 'creation_date')->widget(DateControl::classname(), [
        'displayFormat' => 'php:Y-m-d H:m:s',
        'type' => DateControl::FORMAT_DATETIME
    ]);
    ?>

    <?= $form->field($model, 'alarm')->dropDownList(['0' => 'Turn Off', '1' => 'Turn Onf']); ?>

    <?=
    $form->field($model, 'id_category')->dropDownList(
            ArrayHelper::map((new \yii\db\Query())->select(['id', 'name'])->from('categories')->all(), 'id', 'name'), ['prompt' => 'Categories']
    )
    ?>

    <?= $form->field($model, 'active')->dropDownList(['0' => 'Disabled', '1' => 'Activated']); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
