<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Chores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'expiration_date',
            'creation_date',
            'alarm:boolean',
            'active:boolean',
//            [
//                'label' => 'Active',
//                'value' => function($model) {
//                    return ($model->active === 1) ? 'Disabled' : 'Activated';
//                }
//            ],
            'user.usuario',
            'category.name',
            [
                'header' => 'View',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
            [
                'header' => 'Update',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
            [
                'header' => 'Delete',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
