<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Chores */

$this->title = 'Create Chores';
$this->params['breadcrumbs'][] = ['label' => 'Chores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
