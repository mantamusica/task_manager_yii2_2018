<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Chores */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Chores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this chore?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'expiration_date',
            'creation_date',
            'alarm:boolean',
            'active:boolean',
//            [
//                'label' => 'Active',
//                'value' => function($model) {
//                    return ($model->active === 1) ? 'Disabled' : 'Activated';
//                }
//            ],
            'id_user',
            'category.name',
        ],
    ])
    ?>

</div>
