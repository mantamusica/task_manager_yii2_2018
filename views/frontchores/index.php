<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Chores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'expiration_date',
            'creation_date',
            'alarm:boolean',
            'active:boolean',
//            [
//                'label' => 'Active',
//                'value' => function($model) {
//                    return ($model->active === 1) ? 'Disabled' : 'Activated';
//                }
//            ],
            'category.name',
            [
                'header' => 'View',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
            [
                'header' => 'Update',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
            [
                'header' => 'Delete',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ],
        ],
    ]);
    ?>
</div>
