<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Chores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_user')->textInput(['readOnly' => true, 'value' => Yii::$app->user->id]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'expiration_date')->widget(DateControl::classname(), [
        'displayFormat' => 'php:Y-m-d H:m:s',
        'type' => DateControl::FORMAT_DATETIME
    ]);
    ?>

    <?=
    $form->field($model, 'creation_date')->widget(DateControl::classname(), [
        'displayFormat' => 'php:Y-m-d H:m:s',
        'type' => DateControl::FORMAT_DATETIME
    ]);
    ?>

    <?= $form->field($model, 'alarm')->dropDownList(['0' => 'Turn On', '1' => 'Turn Off']); ?>
    <?=
    $form->field($model, 'id_category')->dropDownList(
            ArrayHelper::map((new \yii\db\Query())->select(['id', 'name'])->from('categories')->all(), 'id', 'name'), ['prompt' => 'Categories']
    )
    ?>
    <?= $form->field($model, 'active')->dropDownList(['0' => 'Actived', '1' => 'Disabled']); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
