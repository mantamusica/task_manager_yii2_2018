<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'usuario',
            'email:email',
            'active:boolean',
//            [
//                'label' => 'Active',
//                'value' => function($model) {
//                    return ($model->active === 1) ? 'Disabled' : 'Activated';
//                }
//            ],
            [
                'header' => 'View',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
            [
                'header' => 'Update',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
            [
                'header' => 'Delete',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
            ], [
                'header' => 'Contact',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{contact}',
                'buttons' => [
                    'contact' => function($url, $model) {
                        return Html::a('<span class="fa fa-envelope fa-2x"></span>', ['back/contact', 'id' => $model->id]);
                    },
                ],
            ],
        ],
    ]);
    ?>
</div>
