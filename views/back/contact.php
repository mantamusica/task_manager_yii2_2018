<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('Email')) { ?>

        <div class="alert alert-success">

            We can get in touch with you as soon as possible
        </div>

    <?php } else { ?>

        <p>
            To contact us send us a form
        </p>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']);
                ?>

                <?= $form->field($model, 'name')->textInput(['readOnly' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['readOnly' => true]) ?>

                <?= $form->field($model, 'subject') ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

                <?=
                $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'captchaAction' => ['/back/captcha'],
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    'imageOptions' => [
                        'id' => 'my-captcha-image'
                    ],
                    'options' => [
                        'placeholder' => 'Write the text'
                    ]
                ])
                ?>
                <div class="form-group">
                    <?php
                    echo Html::tag('div', 'Change the image', [
                        'id' => 'refresh-captcha',
                        'class' => 'btn btn-info btn-xs'
                    ]);
                    ?>
                    <?php $this->registerJs("
            $('#refresh-captcha').on('click', function(e){
                e.preventDefault();
                $('#my-captcha-image').yiiCaptcha('refresh');
            })
        ");
                    ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php } ?>
</div>
