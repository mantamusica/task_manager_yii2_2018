<?php

use app\components\Foto;

/* @var $this yii\web\View */
\app\assets\Animate::register($this);
$this->title = 'Task Manager 1.3';
?>
<div class="jumbotron">
    <span id="animationSandbox" style="display: block;" class="animated zoomIn">
    <h1 class="site_little mega">Task Manager</h1>
    </span>
    <div class="body-content">
    </div>
    <div class="row">
        <div class="col-md-12">
<?= Foto::widget(); ?>
        </div>
    </div>
</div>

